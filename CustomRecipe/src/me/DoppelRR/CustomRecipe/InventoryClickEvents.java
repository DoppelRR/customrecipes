package me.DoppelRR.CustomRecipe;

import me.DoppelRR.CustomRecipe.CustomRecipe.RecipeType;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class InventoryClickEvents implements Listener {
	
	private CustomRecipe plugin;
	
	public InventoryClickEvents(CustomRecipe plugin) {
		this.plugin = plugin;
	}
	
	@EventHandler
	public void onClick(InventoryClickEvent e) {
		
		Inventory editor = e.getInventory();
		
		//check if the inventory is the Editor GUI
		if (editor.getTitle() == null || !editor.getName().equalsIgnoreCase("Editor"))
			return;
		
		ItemStack clicked = e.getCurrentItem();

		
		//do nothing if a placeholder is clicked
		if (clicked.getType() == Material.STAINED_GLASS_PANE) {
			e.setCancelled(true);
			return;
		}
		
		//check if item has a name
		if (!clicked.hasItemMeta())
			return;
		if (!clicked.getItemMeta().hasDisplayName())
			return;
		
		//check if the save "Button" was clicked
		if (clicked.getItemMeta().getDisplayName().equalsIgnoreCase("save")) {
			
			//the Recipes.yml"
			ConfigAccessor ca = new ConfigAccessor(plugin, "Recipes.yml");
			
			//the interacting player
			Player p = (Player) e.getWhoClicked();
			
			ItemStack sort = editor.getItem(41);
			ItemStack product = editor.getItem(25);
			
			//check if the product is null
			if (product == null) {
				e.setCancelled(true);
				return;
			}
			
			if (sort.getType() == Material.DIAMOND_PICKAXE) {
				
				//all set ItemStacks
				ItemStack m1 = editor.getItem(12);
				ItemStack m2 = editor.getItem(13);
				ItemStack m3 = editor.getItem(14);
				ItemStack m4 = editor.getItem(21);
				ItemStack m5 = editor.getItem(22);
				ItemStack m6 = editor.getItem(23);
				ItemStack m7 = editor.getItem(30);
				ItemStack m8 = editor.getItem(31);
				ItemStack m9 = editor.getItem(32);
				
				//save everything
				ca.getConfig().set(CustomRecipe.currentFile.get(p) + ".Slot1", m1);
				ca.getConfig().set(CustomRecipe.currentFile.get(p) + ".Slot2", m2);
				ca.getConfig().set(CustomRecipe.currentFile.get(p) + ".Slot3", m3);
				ca.getConfig().set(CustomRecipe.currentFile.get(p) + ".Slot4", m4);
				ca.getConfig().set(CustomRecipe.currentFile.get(p) + ".Slot5", m5);
				ca.getConfig().set(CustomRecipe.currentFile.get(p) + ".Slot6", m6);
				ca.getConfig().set(CustomRecipe.currentFile.get(p) + ".Slot7", m7);
				ca.getConfig().set(CustomRecipe.currentFile.get(p) + ".Slot8", m8);
				ca.getConfig().set(CustomRecipe.currentFile.get(p) + ".Slot9", m9);
				ca.getConfig().set(CustomRecipe.currentFile.get(p) + ".Product", product);
				ca.getConfig().set(CustomRecipe.currentFile.get(p) + ".Enabled", true);
				ca.getConfig().set(CustomRecipe.currentFile.get(p) + ".Type", "Shaped");
			} else if (sort.getType() == Material.WOOD_PICKAXE) {
				
				//all set ItemStacks
				ItemStack m1 = editor.getItem(12);
				ItemStack m2 = editor.getItem(13);
				ItemStack m3 = editor.getItem(14);
				ItemStack m4 = editor.getItem(21);
				ItemStack m5 = editor.getItem(22);
				ItemStack m6 = editor.getItem(23);
				ItemStack m7 = editor.getItem(30);
				ItemStack m8 = editor.getItem(31);
				ItemStack m9 = editor.getItem(32);
				
				//save everything
				ca.getConfig().set(CustomRecipe.currentFile.get(p) + ".Slot1", m1);
				ca.getConfig().set(CustomRecipe.currentFile.get(p) + ".Slot2", m2);
				ca.getConfig().set(CustomRecipe.currentFile.get(p) + ".Slot3", m3);
				ca.getConfig().set(CustomRecipe.currentFile.get(p) + ".Slot4", m4);
				ca.getConfig().set(CustomRecipe.currentFile.get(p) + ".Slot5", m5);
				ca.getConfig().set(CustomRecipe.currentFile.get(p) + ".Slot6", m6);
				ca.getConfig().set(CustomRecipe.currentFile.get(p) + ".Slot7", m7);
				ca.getConfig().set(CustomRecipe.currentFile.get(p) + ".Slot8", m8);
				ca.getConfig().set(CustomRecipe.currentFile.get(p) + ".Slot9", m9);
				ca.getConfig().set(CustomRecipe.currentFile.get(p) + ".Product", product);
				ca.getConfig().set(CustomRecipe.currentFile.get(p) + ".Enabled", true);
				ca.getConfig().set(CustomRecipe.currentFile.get(p) + ".Type", "Shapeless");
			} else if (sort.getType() == Material.FURNACE) {
				//all set ItemStacks
				ItemStack input = editor.getItem(22);
				
				//save everything
				ca.getConfig().set(CustomRecipe.currentFile.get(p) + ".Input", input);
				ca.getConfig().set(CustomRecipe.currentFile.get(p) + ".Product", product);
				ca.getConfig().set(CustomRecipe.currentFile.get(p) + ".Enabled", true);
				ca.getConfig().set(CustomRecipe.currentFile.get(p) + ".Type", "Furnace");
			}
			ca.saveConfig();
			
			e.setCancelled(true);
			p.closeInventory();

			//reload all recipes
			plugin.onDisable();
			plugin.onEnable();
			p.sendMessage(ChatColor.GREEN + "The recipes have been reloaded.");
			return;
		} else if (clicked.getItemMeta().getDisplayName().equals("Shapeless Recipe")) {
			ItemStack sort = new ItemStack(Material.DIAMOND_PICKAXE);
			ItemMeta sortMeta = sort.getItemMeta();
			sortMeta.setDisplayName("Shaped Recipe");
			sort.setItemMeta(sortMeta);
			editor.setItem(41, sort);
			e.setCancelled(true);
		} else if (clicked.getItemMeta().getDisplayName().equals("Shaped Recipe")) {
			ItemStack sort = new ItemStack(Material.FURNACE);
			ItemMeta sortMeta = sort.getItemMeta();
			sortMeta.setDisplayName("Furnace Recipe");
			sort.setItemMeta(sortMeta);
			e.getWhoClicked().closeInventory();
			e.getWhoClicked().openInventory(plugin.createGUI(RecipeType.FURNACE));
			e.setCancelled(true);
		} else if (clicked.getItemMeta().getDisplayName().equals("Furnace Recipe")) {
			ItemStack sort = new ItemStack(Material.WOOD_PICKAXE);
			ItemMeta sortMeta = sort.getItemMeta();
			sortMeta.setDisplayName("Shapeless Recipe");
			sort.setItemMeta(sortMeta);
			Player p = (Player) e.getWhoClicked();
			p.closeInventory();
			p.openInventory(plugin.createGUI(RecipeType.SHAPELESS));
			e.setCancelled(true);
		}
	}
}
