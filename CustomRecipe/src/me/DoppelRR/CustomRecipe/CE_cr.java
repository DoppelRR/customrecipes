package me.DoppelRR.CustomRecipe;

import me.DoppelRR.CustomRecipe.CustomRecipe.RecipeType;

import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

public class CE_cr implements CommandExecutor {
	
	private CustomRecipe plugin;
	
	public CE_cr(CustomRecipe plugin) {
		this.plugin = plugin;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label,
			String[] args) {
		if(!(sender instanceof Player)) {
			System.out.println("You must be a player to do that");
			return true;
		}
		Player p = (Player) sender;
		if(label.equalsIgnoreCase("cr")) {
			if(p.hasPermission("customrecipe.command")) {
			if(args.length == 0) {
				p.sendMessage(ChatColor.YELLOW + "/cr create <recipe>" + ChatColor.WHITE + ": Will create new recipe");
				p.sendMessage(ChatColor.YELLOW + "/cr remove <recipe>" + ChatColor.WHITE + ": Will remove existed recipe");
				p.sendMessage(ChatColor.YELLOW + "/cr list" + ChatColor.WHITE + ": list all created recipes");
				p.sendMessage(ChatColor.YELLOW + "/cr edit <recipe>" + ChatColor.WHITE + ": edit existed recipe");
				p.sendMessage(ChatColor.YELLOW + "/cr disable <recipe>" + ChatColor.WHITE + ": Will disable existed recipe");
				p.sendMessage(ChatColor.YELLOW + "/cr enable <recipe>" + ChatColor.WHITE + ": Will enable existed recipe");
				return true;
			}
			}else {
				p.sendMessage(ChatColor.RED + "You have no permission to do that");
			}
			if(args.length >= 2) {
		if(args[0].equalsIgnoreCase("create")) {
			
			//check for permission
			if (!p.hasPermission("customrecipe.create")) {
				p.sendMessage(ChatColor.RED + "You have no permission to do that");
				return true;
			}
			
			//check correct usage
			if (args.length != 2) {
				return false;
			}
			
			//the Recipes.yml
			ConfigAccessor ca = new ConfigAccessor(this.plugin, "Recipes.yml");
			
			//the recipes name
			String name = args[1];
			
			//check if the recipe exists
			if (ca.getConfig().contains(name)) {
				p.sendMessage(ChatColor.RED + "Sorry, but this recipe already exists, use /cr edit " + name + " to edit it");
				return true;
			}
			
			Inventory editor = plugin.createGUI(RecipeType.SHAPED);
			
			//open the inventory
			p.openInventory(editor);

			//play sound
			p.playSound(p.getLocation(), Sound.NOTE_PLING, 1.0F, 1.0F);
			
			//store the recipe name
			CustomRecipe.currentFile.put(p, name);
			return true;
			}
			if(args[0].equalsIgnoreCase("remove")) {
				//check if the player has the correct permission
				if (!p.hasPermission("customrecipe.remove")) {
					p.sendMessage(ChatColor.RED + "You have no permission to do that");
					return true;
				}
				
				//check if the command was used correctly
				if (args.length != 2)
					return false;
				
				//the Recipes.yml
				ConfigAccessor ca = new ConfigAccessor(this.plugin, "Recipes.yml");
				
				//the recipes name
				String name = args[1];
				
				//check if the recipe exists
				if (!ca.getConfig().contains(name)) {
					p.sendMessage(ChatColor.RED + "Recipe given does not exists");
					return true;
				}
				
				//remove the recipe
				ca.getConfig().set(name, null);
				ca.saveConfig();
				p.sendMessage(ChatColor.GREEN + "The recipe was removed successfully.");
				return true;
			}
			if(args[0].equalsIgnoreCase("list")) {
				
				//check if the player has ther correct permission
				if (!p.hasPermission("customrecipe.list")) {
					p.sendMessage(ChatColor.RED + "You have no permission to do that");
					return true;
				}
				
				//check if the command was used correctly
				if (args.length != 1) {
					return false;
				}
				
				//the Recipes.yml
				ConfigAccessor ca = new ConfigAccessor(this.plugin, "Recipes.yml");		
				
				//send all recipe names
				for (String cs : ca.getConfig().getKeys(false)) {
					p.sendMessage(cs);
					return true;
				}
			}
			if(args[0].equalsIgnoreCase("enable")) {
				
				//check if the player has the correct permission
				if (!p.hasPermission("customrecipe.enable")) {
					p.sendMessage(ChatColor.RED + "You have no permission to do that");
					return true;
				}
				
				//check if the command was used correctly
				if (args.length != 2)
					return false;
				
				//the Recipes.yml
				ConfigAccessor ca = new ConfigAccessor(this.plugin, "Recipes.yml");
				
				//the recipes name
				String name = args[1];
				
				//check if the recipe exists
				if (!ca.getConfig().contains(name)) {
					p.sendMessage(ChatColor.RED + "Recipe is not existed use /cr create <recipe> to create new one");
					return true;
				}
				
				//enable the recipe
				ca.getConfig().set(name + ".Enabled", true);
				ca.saveConfig();
				
				//reload recipes
				this.plugin.onDisable();
				this.plugin.onEnable();
				
				p.sendMessage(ChatColor.GREEN + "Enabled the recipe.");
				return true;
			}
			if(args[0].equalsIgnoreCase("disable")) {
				
				//check if the player has the correct permissions
				if (!p.hasPermission("customrecipe.disable")) {
					p.sendMessage(ChatColor.RED + "You have no permission to do that");
					return true;
				}
				
				//check if the command was used correctly
				if (args.length != 2)
					return false;
				
				//the Recipes.yml
				ConfigAccessor ca = new ConfigAccessor(this.plugin, "Recipes.yml");
				
				//the recipes name
				String name = args[1];
				
				//check if the recipe exists
				if (!ca.getConfig().contains(name)) {
					p.sendMessage(ChatColor.RED + "Recipe is not existed use /cr create <recipe> to create new one");
					return true;
				}
				
				//disable the recipe
				ca.getConfig().set(name + ".Enabled", false);
				ca.saveConfig();
				
				//reload all recipes
				this.plugin.onDisable();
				this.plugin.onEnable();
				
				p.sendMessage(ChatColor.GREEN + "Disabled the recipe.");
				return true;
			}
			if(args[0].equalsIgnoreCase("edit")) {
				
				//check if the player has the permission to perform this command
				if (!p.hasPermission("customrecipe.edit")) {
					p.sendMessage(ChatColor.RED + "You have no permission to do that");
					return true;
				}
				
				//check if the command is used correctly
				if (args.length != 2)
					return false;
				
				//the Recipes.yml
				ConfigAccessor ca = new ConfigAccessor(plugin, "Recipes.yml");
				
				//the name of the recipe
				String name = args[1];
				
				//check if this recipe exists
				if (!ca.getConfig().contains(name)) {
					p.sendMessage(ChatColor.RED + "Sorry, but this recipe does not exist, use /cr create " + name + " to create it");
					return true;
				}
				
				//Check which GUI is needed
				Inventory editor = null;
				String type = ca.getConfig().getString(name + ".Type");
				if (type.equals("Shaped")) {
					editor = plugin.createGUI(RecipeType.SHAPED);
				} else if (type.equals("Shapeless")) {
					editor = plugin.createGUI(RecipeType.SHAPELESS);
				} else if (type.equals("Furnace")) {
					editor = plugin.createGUI(RecipeType.FURNACE);
				}
				
				//open the inventory
				p.openInventory(editor);
				
				//store the recipe name
				CustomRecipe.currentFile.put(p, name);
			}
			return true;
		}
		}
		return false;
	}			
	
}
