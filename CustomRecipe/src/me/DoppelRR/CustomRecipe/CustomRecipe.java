package me.DoppelRR.CustomRecipe;

import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.FurnaceRecipe;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.inventory.ShapelessRecipe;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.material.Wool;
import org.bukkit.plugin.java.JavaPlugin;

public class CustomRecipe extends JavaPlugin {
	
	public static HashMap<Player, String> currentFile = new HashMap<Player, String>();
	
	@Override
	public void onEnable() {
		
		//register the EventHandler and the /cr command
		this.getServer().getPluginManager().registerEvents(new InventoryClickEvents(this), this);
		this.getCommand("cr").setExecutor(new CE_cr(this));
		
		//load all recipes
		ConfigAccessor crs = new ConfigAccessor(this, "Recipes.yml");
		for (String cs : crs.getConfig().getKeys(false)) {
			//check if the recipe is enabled
			if (crs.getConfig().getBoolean(cs + ".Enabled")) {
				//check if the recipe is shaped
				if (crs.getConfig().getString(cs + ".Type").equals("Shaped")) {
					//the new recipe
					ShapedRecipe recipe = new ShapedRecipe(crs.getConfig().getItemStack(cs + ".Product"));
					
					//the recipes shape
					recipe.shape("123", "456", "789");
					
					for (Integer i = 1; i <= 9; i++) {
						try {
							recipe.setIngredient(i.toString().toCharArray()[0], crs.getConfig().getItemStack(cs + ".Slot" + i).getData());
						} catch (NullPointerException npe) {} //A NPE is thrown when the certain slot would be nothing
					}
					Bukkit.getServer().addRecipe(recipe);
				} else if (crs.getConfig().getString(cs + ".Type").equals("Shapeless")) {
					//the new recipe
					ShapelessRecipe recipe = new ShapelessRecipe(crs.getConfig().getItemStack(cs + ".Product"));
					
					for (Integer i = 1; i <= 9; i++) {
						try {
							recipe.addIngredient(crs.getConfig().getItemStack(cs + ".Slot" + i).getData());
						} catch (NullPointerException npe) {} //A NPE is thrown when the certain slot would be nothing
					}
					Bukkit.getServer().addRecipe(recipe);
				} else if (crs.getConfig().getString(cs + ".Type").equals("Furnace")) {
					
					//the new Recipe
					FurnaceRecipe recipe = new FurnaceRecipe(crs.getConfig().getItemStack(cs + ".Product"),
							crs.getConfig().getItemStack(cs + ".Input").getData());
					
					Bukkit.getServer().addRecipe(recipe);
					
				}
			}
		}
		
		System.out.println("[CustomRecipes] The plugin has been enabled successfully, and all recipes have been loaded.");
	}
	
	@Override
	public void onDisable() {
		System.out.println("[CustomRecipes] The plugin has been disabled successfully.");
	}
	
	public Inventory createGUI(RecipeType type) {
		
		if (type == RecipeType.SHAPED) {
			//the save "Button"
			ItemStack save = new Wool(DyeColor.GREEN).toItemStack();
			ItemMeta saveMeta = save.getItemMeta();
			saveMeta.setDisplayName("SAVE");
			save.setItemMeta(saveMeta);
			
			//the sort "Button" which shows and changes if a recipe is shaped or not
			ItemStack sort = new ItemStack(Material.DIAMOND_PICKAXE);
			ItemMeta sortMeta = sort.getItemMeta();
			sortMeta.setDisplayName("Shaped Recipe");
			sort.setItemMeta(sortMeta);
			
			//a placeholder
			ItemStack empty = new ItemStack(Material.STAINED_GLASS_PANE);
			
			//the GUI
			Inventory editor = Bukkit.createInventory(null, 45, "Editor");
			
			//place the placeholder everywhere
			for (int i = 0; i < editor.getSize(); i++) {
				editor.setItem(i, empty);
			}
			
			//add all "special" items
			editor.setItem(12, new ItemStack(Material.AIR));
			editor.setItem(13, new ItemStack(Material.AIR));
			editor.setItem(14, new ItemStack(Material.AIR));
			editor.setItem(21, new ItemStack(Material.AIR));
			editor.setItem(22, new ItemStack(Material.AIR));
			editor.setItem(23, new ItemStack(Material.AIR));
			editor.setItem(25, new ItemStack(Material.AIR));
			editor.setItem(30, new ItemStack(Material.AIR));
			editor.setItem(31, new ItemStack(Material.AIR));
			editor.setItem(32, new ItemStack(Material.AIR));
			editor.setItem(43, save);
			editor.setItem(41, sort);
			return editor;
		} else if (type == RecipeType.SHAPELESS) {
			//the save "Button"
			ItemStack save = new Wool(DyeColor.GREEN).toItemStack();
			ItemMeta saveMeta = save.getItemMeta();
			saveMeta.setDisplayName("SAVE");
			save.setItemMeta(saveMeta);
			
			//the sort "Button" which shows and changes if a recipe is shaped or not
			ItemStack sort = new ItemStack(Material.WOOD_PICKAXE);
			ItemMeta sortMeta = sort.getItemMeta();
			sortMeta.setDisplayName("Shapeless Recipe");
			sort.setItemMeta(sortMeta);
			
			//a placeholder
			ItemStack empty = new ItemStack(Material.STAINED_GLASS_PANE);
			
			//the GUI
			Inventory editor = Bukkit.createInventory(null, 45, "Editor");
			
			//place the placeholder everywhere
			for (int i = 0; i < editor.getSize(); i++) {
				editor.setItem(i, empty);
			}
			
			//add all "special" items
			editor.setItem(12, new ItemStack(Material.AIR));
			editor.setItem(13, new ItemStack(Material.AIR));
			editor.setItem(14, new ItemStack(Material.AIR));
			editor.setItem(21, new ItemStack(Material.AIR));
			editor.setItem(22, new ItemStack(Material.AIR));
			editor.setItem(23, new ItemStack(Material.AIR));
			editor.setItem(25, new ItemStack(Material.AIR));
			editor.setItem(30, new ItemStack(Material.AIR));
			editor.setItem(31, new ItemStack(Material.AIR));
			editor.setItem(32, new ItemStack(Material.AIR));
			editor.setItem(43, save);
			editor.setItem(41, sort);
			return editor;
		} else if (type == RecipeType.FURNACE) {
			//the save "Button"
			ItemStack save = new Wool(DyeColor.GREEN).toItemStack();
			ItemMeta saveMeta = save.getItemMeta();
			saveMeta.setDisplayName("SAVE");
			save.setItemMeta(saveMeta);
			
			//the sort "Button" which shows and changes if a recipe is shaped or not
			ItemStack sort = new ItemStack(Material.FURNACE);
			ItemMeta sortMeta = sort.getItemMeta();
			sortMeta.setDisplayName("Furnace Recipe");
			sort.setItemMeta(sortMeta);
			
			//a placeholder
			ItemStack empty = new ItemStack(Material.STAINED_GLASS_PANE);
			
			//the GUI
			Inventory editor = Bukkit.createInventory(null, 45, "Editor");
			
			//place the placeholder everywhere
			for (int i = 0; i < editor.getSize(); i++) {
				editor.setItem(i, empty);
			}
			
			//add all "special" items
			editor.setItem(22, new ItemStack(Material.AIR));
			editor.setItem(25, new ItemStack(Material.AIR));
			editor.setItem(43, save);
			editor.setItem(41, sort);
			return editor;
		}
		
		return null;
	}
	
	public enum RecipeType {
		SHAPELESS,
		SHAPED,
		FURNACE
	}
}
